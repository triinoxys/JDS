package uk.co.hexeptionclients.jds.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.client.Minecraft;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.friend.FriendManager;
import uk.co.hexeptionclients.jds.module.Module;
import uk.co.hexeptionclients.jds.module.Module.Category;

public class FileManager {
	
	public final File JDSDir = new File(Minecraft.getMinecraft().mcDataDir, "JDS");
	
	public final File modules = new File(JDSDir, "modules.json");
	public final File keybinds = new File(JDSDir, "keybinds.json");
	public final File friends = new File(JDSDir, "friends.json");
	
	public static Gson gson = new Gson();
	public static Gson GsonPretty = new GsonBuilder().setPrettyPrinting().create();
	public static JsonParser jsonParser = new JsonParser();
	
	public void init() {
		if (!JDSDir.exists()) {
			JDSDir.mkdir();
		}
		
		if (!modules.exists()) {
			saveMods();
		}else{
			loadMods();
		}
		
		if (!keybinds.exists()) {
			
		}
		
		if(!friends.exists()){
			saveFriends();
		}else{
			loadFriends();
		}
	}
	
	public void saveMods() {
		try {
			JsonObject json = new JsonObject();
			for (Module mod : JDS.theClient.moduleManager.getAllMods()) {
				JsonObject jsonMods = new JsonObject();
				jsonMods.addProperty("enabled", mod.getState());
				json.add(mod.getName(), jsonMods);
			}
			PrintWriter savejson = new PrintWriter(new FileWriter(modules));
			savejson.println(GsonPretty.toJson(json));
			savejson.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadMods(){
		try{
			BufferedReader loadJson = new BufferedReader(new FileReader(modules));
			JsonObject moduleJson = (JsonObject) jsonParser.parse(loadJson);
			loadJson.close();
			Iterator<Entry<String, JsonElement>> itr = moduleJson.entrySet().iterator();
			while(itr.hasNext()){
				Entry<String, JsonElement> entry = itr.next();
				Module mod = JDS.theClient.moduleManager.getModByName(entry.getKey());
				if(mod != null && mod.getCategory() != Category.GUI){
					JsonObject jsonmod = (JsonObject) entry.getValue();
					boolean enabled = jsonmod.get("enabled").getAsBoolean();
					if(enabled){
						mod.setState(true);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveFriends(){
		try{
			PrintWriter save = new PrintWriter(new FileWriter(friends));
			save.println(GsonPretty.toJson(JDS.theClient.friendManager));
			save.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadFriends(){
		try{
			BufferedReader load = new BufferedReader(new FileReader(friends));
			JDS.theClient.friendManager = gson.fromJson(load, FriendManager.class);
			load.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
