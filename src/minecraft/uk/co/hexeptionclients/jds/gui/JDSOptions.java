package uk.co.hexeptionclients.jds.gui;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.gui.keybinds.GuiKeybindManager;

public class JDSOptions extends GuiScreen {
	
	private GuiScreen lastScreen;
	private GuiButton lastMousedButton;
	
	public JDSOptions(GuiScreen lastscreen) {
		this.lastScreen = lastscreen;
	}
	
	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		this.buttonList.clear();
		
		int leftAlign = this.width / 2 - 152;
		int rightAlign = this.width / 2 + 2;
		int topExtraTop = this.height / 6 - 12;
		int topTop = this.height / 6 - 12 + 24;
		
		this.buttonList.add(new GuiButton(1, leftAlign, topTop, 150, 20, "Theme: " + JDS.theClient.hud.getCurrentTheme().themeName()));
		this.buttonList.add(new GuiButton(2, rightAlign, topTop, 150, 20, "Keybind Manager"));
		
		this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 168, "Done"));
		
		super.initGui();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		this.lastMousedButton = button;
		
		switch (button.id) {
			case 0:
				this.mc.displayGuiScreen(this.lastScreen);
				break;
			case 1:
				JDS.theClient.hud.onNextTheme();
				initGui();
				break;
			case 2:
				this.mc.displayGuiScreen(new GuiKeybindManager(this));
				break;
		}
	}
	
	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		drawCenteredString(fontRendererObj, "JDS Options", this.width / 2, 16, 16777215);
	}
	
}
