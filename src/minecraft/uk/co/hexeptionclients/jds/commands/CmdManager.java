package uk.co.hexeptionclients.jds.commands;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeMap;

import uk.co.hexeptionclients.jds.commands.Cmd.SyntaxError;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public class CmdManager {
	
	private final TreeMap<String, Cmd> cmds = new TreeMap<String, Cmd>(new Comparator<String>() {
		
		@Override
		public int compare(String o1, String o2) {
			return o1.compareToIgnoreCase(o2);
		}
	});
	
	public final TCmd tCmd = new TCmd();
	public final FriendCmd friendCmd = new FriendCmd();
	
	public CmdManager() {
		try {
			for (Field field : CmdManager.class.getFields()) {
				if (field.getName().endsWith("Cmd")) {
					Cmd cmd = (Cmd) field.get(this);
					cmds.put(cmd.getCmdName(), cmd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void SendCommand(String message) {
		String input = message.substring(1);
		String commandName = input.split(" ")[0];
		String[] args;
		if (input.contains(" ")) {
			args = input.substring(input.indexOf(" ") + 1).split(" ");
		} else {
			args = new String[0];
		}
		
		Cmd cmd = getCommandByName(commandName);
		if (cmd != null) {
			try {
				cmd.execute(args);
			} catch (SyntaxError e) {
				if (e.getMessage() != null)
					Wrapper.getInstance().addChatMessage("�4Syntax error:�r " + e.getMessage());
				else Wrapper.getInstance().addChatMessage("�4Syntax error!�r");
				cmd.printSyntax();
			} catch (Cmd.Error e) {
				Wrapper.getInstance().addChatMessage(e.getMessage());
			}
		}
	}
	
	public Cmd getCommandByName(String name) {
		return cmds.get(name);
	}
	
	public Collection<Cmd> getAllCommands() {
		return cmds.values();
	}
	
	public int countCommands() {
		return cmds.size();
	}
	
}
