package uk.co.hexeptionclients.jds.module;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Random;

import com.darkmagician6.eventapi.EventManager;

import uk.co.hexeptionclients.jds.JDS;

public class Module {
	
	private String name = getClass().getAnnotation(Info.class).name();
	private String description = getClass().getAnnotation(Info.class).desciption();
	private Category category = getClass().getAnnotation(Info.class).category();
	private int color;
	private boolean isEnabled;
	
	public enum Category {
		COMBAT, EXPLOITS, RENDER, MISC, PLAYER, MOVMENT, GUI;
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Info {
		String name();
		
		String desciption();
		
		Category category();
		
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Category getCategory() {
		return category;
	}
	
	public int getColor() {
		return color;
	}
	
	public boolean getState() {
		return isEnabled;
	}
	
	public void setState(boolean state) {
		onToggle();
		
		if (state) {
			onEnable();
			this.isEnabled = true;
			EventManager.register(this);
		} else {
			onDisable();
			this.isEnabled = false;
			EventManager.unregister(this);
		}
		
		JDS.theClient.fileManager.saveMods();
	}
	
	public void onDisable() {
		
	}
	
	public void onEnable() {
		Random randomColor = new Random();
		StringBuilder sb = new StringBuilder();
		sb.append("0x");
		while (sb.length() < 10) {
			sb.append(Integer.toHexString(randomColor.nextInt()));
		}
		sb.setLength(8);
		this.color = Integer.decode(sb.toString()).intValue();
	}
	
	
	public void onToggle() {
		
	}
	
	public void toggle() {
		setState(!this.getState());
	}

	
}
